(function ($) {
    Drupal.behaviors.myModuleBehavior = {
        attach: function (context, settings) {

        //START SCRIPT

            //FUNCTION TEMPLATES

                //wowjs and animate.css
                function Animate_View_Rows(blockName, animationName, delay){
                    $(blockName + " .view-content .views-row:nth-child(1)").addClass("wow animated " + animationName).attr("data-wow-delay", 1 * delay + "s");
                    $(blockName + " .view-content .views-row:nth-child(2)").addClass("wow animated " + animationName).attr("data-wow-delay", 2 * delay + "s");
                    $(blockName + " .view-content .views-row:nth-child(3)").addClass("wow animated " + animationName).attr("data-wow-delay", 3 * delay + "s");
                    $(blockName + " .view-content .views-row:nth-child(4)").addClass("wow animated " + animationName).attr("data-wow-delay", 4 * delay + "s");
                }
            
            //MAIN SCRIPT
            Animate_View_Rows(".home-baiviet", "flipInY", 0.2);

            $(".home-link .view-content .views-row:nth-child(1)").addClass("wow animated fadeInUp").attr("data-wow-delay", "0.0s");
            $(".home-link .view-content .views-row:nth-child(2)").addClass("wow animated fadeInLeft").attr("data-wow-delay", "0.0s");
            $(".home-link .view-content .views-row:nth-child(3)").addClass("wow animated fadeInRight").attr("data-wow-delay", "0.0s");
            $(".home-link .view-content .views-row:nth-child(4)").addClass("wow animated fadeInUp").attr("data-wow-delay", "0.0s");

            $(".navbar .navbar-header .logo").addClass("wow animated fadeInLeft").attr("data-wow-delay","0.0s");
            $(".footerinfo").addClass("wow animated fadeInLeft").attr("data-wow-delay","0.0s");
            //$(".navbar .navbar-collapse").addClass("wow animated fadeInRight").attr("data-wow-delay","0.0s");
            $(".slideshow").addClass("wow animated fadeInRight").attr("data-wow-delay","0.0s");

            $(".xem-cau-tra-loi").click(function(){
                $(this).parent().parent().siblings(".views-field-body").slideDown();
            });

        //END SCRIPT
        }
    };
})(jQuery);